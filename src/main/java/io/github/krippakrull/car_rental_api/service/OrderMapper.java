package io.github.krippakrull.car_rental_api.service;


import io.github.krippakrull.car_rental_api.entity.Order;
import io.github.krippakrull.car_rental_api.entity.OrderDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface OrderMapper {
    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    OrderDto entityToDto(Order order);
    Order dtoToEntity(OrderDto orderDto);
    List<OrderDto> entityListToDtoList(List<Order> order);
    List<Order> dtoListToEntityList(List<OrderDto> orderDtoList);
}
