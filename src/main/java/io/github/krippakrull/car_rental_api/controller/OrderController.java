package io.github.krippakrull.car_rental_api.controller;

import io.github.krippakrull.car_rental_api.entity.Order;
import io.github.krippakrull.car_rental_api.entity.OrderDto;
import io.github.krippakrull.car_rental_api.entity.SimpleOrderDTO;
import io.github.krippakrull.car_rental_api.service.OrderMapper;
import io.github.krippakrull.car_rental_api.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1")
@RestController
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OrderDto> getAll() {
        return orderService.getAll();
    }

    @GetMapping(value = "/orders/customer/{id}")
    public List<OrderDto> getByCustomerId(@PathVariable long id) {
        return orderService.getByCustomerId(id);
    }

    @GetMapping(value = "/orders/{id}")
    public OrderDto getById(@PathVariable long id) {
        return orderService.getSingleOrder(id);
    }

    @PostMapping(value = "/orders")
    public void orderCar(@RequestBody OrderDto order) {
        orderService.saveOrder(order);
    }

    @PutMapping(value = "/orders/{id}")
    public void cancelOrder(@PathVariable long id) {
        orderService.cancelOrder(id);
    }

}