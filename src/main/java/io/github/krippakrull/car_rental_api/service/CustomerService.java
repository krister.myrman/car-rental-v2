package io.github.krippakrull.car_rental_api.service;

import io.github.krippakrull.car_rental_api.entity.CustomerDto;
import io.github.krippakrull.car_rental_api.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<CustomerDto> getAll() {
        return CustomerMapper.INSTANCE.entityListToDtoList(customerRepository.findAll());
    }

    public CustomerDto getSingleCustomer(long id) {
        return CustomerMapper.INSTANCE.entityToDto(customerRepository.getById(id));
    }

    public void createCustomer(CustomerDto customer) {
        customerRepository.save(CustomerMapper.INSTANCE.dtoToEntity(customer));
    }

    public void deleteCustomer(long id) {
        customerRepository.deleteById(id);
    }
}
