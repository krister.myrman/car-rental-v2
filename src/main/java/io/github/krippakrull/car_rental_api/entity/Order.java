package io.github.krippakrull.car_rental_api.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode
@Table(name = "`order`")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
//    @JsonBackReference(value = "order-customer")
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @ManyToOne
//    @JsonBackReference(value = "order-car")
    @JoinColumn(name = "car_id", nullable = false)
    private Car car;

    @Column(name = "start_time")
    private LocalDateTime startTime;

    @Column(name = "end_time")
    private LocalDateTime endTime;

    @Column(name = "active")
    @ColumnDefault("true")
    private Boolean active = true;
}
