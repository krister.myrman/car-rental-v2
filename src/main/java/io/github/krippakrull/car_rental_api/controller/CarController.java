package io.github.krippakrull.car_rental_api.controller;

import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import io.github.krippakrull.car_rental_api.entity.CarDto;
import io.github.krippakrull.car_rental_api.service.CarService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class CarController {

    private CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping(value = "/cars2", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CarDto> getAvailableCars(@RequestParam(value = "fromDate") String fromDate, @RequestParam(value = "toDate") String toDate) {
        LocalDateTime parsedFromDate = LocalDateTime.parse(fromDate);
        LocalDateTime parsedToDate = LocalDateTime.parse(toDate);
        log.info(parsedFromDate + " " + parsedToDate);
        return carService.getAllByBookingDates(parsedFromDate, parsedToDate);
    }

    @GetMapping(value = "/cars", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CarDto> getAllCars() {
        return carService.getAll();
    }

    @GetMapping(value = "/cars/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CarDto getSingleCar(@PathVariable long id) {
        return carService.getSingleCar(id);
    }

    @PostMapping(value = "/cars")
    public CarDto addCar(@RequestBody CarDto car ) {
        return carService.saveCar(car);
    }

    @PutMapping(value = "/cars/{id}")
    public CarDto updateCar(@RequestBody CarDto car, @PathVariable Long id) {
        return carService.updateCar(car, id);
    }

    @DeleteMapping(value = "/cars/{id}")
    public CarDto deleteCar(@PathVariable long id) {
        return carService.deleteCar(id);
    }

}
