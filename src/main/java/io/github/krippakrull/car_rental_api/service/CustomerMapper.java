package io.github.krippakrull.car_rental_api.service;

import io.github.krippakrull.car_rental_api.entity.Customer;
import io.github.krippakrull.car_rental_api.entity.CustomerDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CustomerMapper {
    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    CustomerDto entityToDto(Customer customer);
    Customer dtoToEntity(CustomerDto customerDto);
    List<CustomerDto> entityListToDtoList(List<Customer> customerList);
    List<Customer> dtoListToEntityList(List<CustomerDto> customerDtoList);
}
