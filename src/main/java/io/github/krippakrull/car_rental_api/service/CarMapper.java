package io.github.krippakrull.car_rental_api.service;


import io.github.krippakrull.car_rental_api.entity.Car;
import io.github.krippakrull.car_rental_api.entity.CarDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CarMapper {
    CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);

    CarDto carToCarDto(Car car);
    Car carDtoToCar(CarDto carDto);
    List<CarDto> carListToDtoList(List<Car> carList);
    List<Car> dtoListToCarList(List<CarDto> carDtoList);
}
