package io.github.krippakrull.car_rental_api.entity;

import lombok.Data;

import java.util.Set;

@Data
public class CarDto {
    private Long id;

    private String name;

    private double dailyRent;

    private Size size;

}
