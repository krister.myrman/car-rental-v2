package io.github.krippakrull.car_rental_api.entity;

import lombok.Getter;

import java.io.Serializable;

@Getter
public enum Size implements Serializable {
    SMALL,
    MEDIUM,
    LARGE
}
