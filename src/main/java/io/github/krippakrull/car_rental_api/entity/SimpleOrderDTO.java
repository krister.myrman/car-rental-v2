package io.github.krippakrull.car_rental_api.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SimpleOrderDTO {
        private Long customerId;
        private Long carId;
        private LocalDateTime startTime;
        private LocalDateTime endTime;
    }