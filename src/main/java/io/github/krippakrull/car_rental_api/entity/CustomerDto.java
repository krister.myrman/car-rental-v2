package io.github.krippakrull.car_rental_api.entity;

import lombok.Data;

import java.util.Set;

@Data
public class CustomerDto {
    private Long id;

    private String firstName;

    private String lastName;

    private String address;

}
