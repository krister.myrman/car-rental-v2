package io.github.krippakrull.car_rental_api.service;

import io.github.krippakrull.car_rental_api.entity.Car;
import io.github.krippakrull.car_rental_api.entity.CarDto;
import io.github.krippakrull.car_rental_api.entity.Order;
import io.github.krippakrull.car_rental_api.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class CarService {

    private CarRepository carRepository;

    @Autowired
    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<CarDto> getAll() {
        return CarMapper.INSTANCE.carListToDtoList(carRepository.findAll());
    }

    public List<CarDto> getAllByBookingDates(LocalDateTime fromDate, LocalDateTime toDate) {
        log.warn(fromDate + " " + toDate);
        List<Car> cars = carRepository.findAll();
        List<Car> filteredCars = cars.stream().filter(car -> car.getOrders().stream().allMatch
                (order -> checkOrderDates(fromDate, toDate, order))).collect(Collectors.toList());
        return CarMapper.INSTANCE.carListToDtoList(filteredCars);
    }

    public boolean checkOrderDates(LocalDateTime fromDate, LocalDateTime toDate, Order order) {
        if (fromDate.isAfter(order.getEndTime()) || toDate.isBefore(order.getStartTime())) {
            return true;
        }
        return false;
    }

    public CarDto getSingleCar(long id) {
        return CarMapper.INSTANCE.carToCarDto(carRepository.getById(id));
    }

    public CarDto updateCar(CarDto car, long id) {
        Optional<Car> carData = carRepository.findById(car.getId());
        if (carData.isPresent()) {
            Car _car = carData.get();
            _car.setName(car.getName());
            _car.setSize(car.getSize());
            _car.setDailyRent(car.getDailyRent());
            return CarMapper.INSTANCE.carToCarDto(carRepository.save(_car));
        }
        else return new CarDto();
    }

    public CarDto saveCar(CarDto car) {
        Car _car = new Car();
        _car.setName(car.getName());
        _car.setSize(car.getSize());
        _car.setDailyRent(car.getDailyRent());
        return CarMapper.INSTANCE.carToCarDto(carRepository.save(_car));
    }

    public CarDto deleteCar(long id) {
        Car car = carRepository.getById(id);
        carRepository.deleteById(id);
        log.info("Car with id " + id + " deleted!");
        return CarMapper.INSTANCE.carToCarDto(car);
    }
}
