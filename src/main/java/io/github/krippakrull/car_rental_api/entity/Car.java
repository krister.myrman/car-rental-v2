package io.github.krippakrull.car_rental_api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "car")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "daily_rent")
    private double dailyRent;

    @Column(name = "size")
    @Enumerated(EnumType.STRING)
    private Size size;

    @JsonIgnore
//    @JsonManagedReference(value = "order-car")
    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    private Set<Order> orders = new HashSet<>();
}
