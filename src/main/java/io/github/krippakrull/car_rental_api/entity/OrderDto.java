package io.github.krippakrull.car_rental_api.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OrderDto {
    private Long id;
    private Customer customer;
    private Car car;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Boolean active;
}
