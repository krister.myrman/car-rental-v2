package io.github.krippakrull.car_rental_api.repository;

import io.github.krippakrull.car_rental_api.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    List<Customer> findCustomersByFirstName(String name);
}
