package io.github.krippakrull.car_rental_api.service;

import io.github.krippakrull.car_rental_api.entity.Car;
import io.github.krippakrull.car_rental_api.entity.Order;
import io.github.krippakrull.car_rental_api.entity.OrderDto;
import io.github.krippakrull.car_rental_api.entity.SimpleOrderDTO;
import io.github.krippakrull.car_rental_api.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class OrderService {

    OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<OrderDto> getAll() {
        return OrderMapper.INSTANCE.entityListToDtoList(orderRepository.findAll());
    }

    public OrderDto getSingleOrder(long id) {
        return OrderMapper.INSTANCE.entityToDto(orderRepository.getById(id));
    }


    public Order saveOrder(OrderDto order) {
        Order _order = new Order();
        _order.setCar(order.getCar());
        _order.setCustomer(order.getCustomer());
        _order.setStartTime(order.getStartTime());
        _order.setEndTime(order.getEndTime());
        return orderRepository.save(_order);
    }

//    public Order saveOrder(OrderDto order) {
//
//        return orderRepository.save(OrderMapper.INSTANCE.dtoToEntity(order));
//    }

    List<OrderDto> getAvailable() {
        return OrderMapper.INSTANCE.entityListToDtoList(orderRepository.findAllByEndTimeBeforeOrStartTimeAfter(LocalDateTime.now(), LocalDateTime.now()));
    }

    public void deleteOrder(long id) {
        orderRepository.deleteById(id);
    }

    public List<OrderDto> getByCustomerId(long id) {return OrderMapper.INSTANCE.entityListToDtoList(orderRepository.getAllByCustomerId(id));}

    public void cancelOrder(long id) {
        Order order = orderRepository.getById(id);
        order.setActive(false);
        orderRepository.save(order);
        log.info("Booking from customer " + order.getCustomer().getId() + " cancelled!");
    }
}
