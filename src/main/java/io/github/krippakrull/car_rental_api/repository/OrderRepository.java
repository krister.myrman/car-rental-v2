package io.github.krippakrull.car_rental_api.repository;

import io.github.krippakrull.car_rental_api.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> getAllByCustomerId(long id);
    List<Order> findAllByEndTimeBeforeOrStartTimeAfter(LocalDateTime bookingStart, LocalDateTime bookingEnd);
}
